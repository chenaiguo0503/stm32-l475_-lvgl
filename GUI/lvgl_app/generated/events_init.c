/*
 * Copyright 2022 NXP
 * SPDX-License-Identifier: MIT
 */

#include "events_init.h"
#include <stdio.h>
#include "lvgl/lvgl.h"
#include "led.h"

extern lv_ui guider_ui;

void events_init(lv_ui *ui)
{
}

uint8_t gauge_value = 0;

static void screen_btn_1event_handler(lv_obj_t * obj, lv_event_t event)
{
	switch (event)
	{
		case LV_EVENT_CLICKED:
		{	
			if (gauge_value > 180) 
				gauge_value = 0;
			
			lv_gauge_set_value(guider_ui.screen_gauge_1, 0, gauge_value);
			gauge_value += 10;
		}
		break;
	default:
		break;
	}
}

static void screen_btn_2event_handler(lv_obj_t * obj, lv_event_t event)
{
	switch (event)
	{
		case LV_EVENT_CLICKED:
		{
			if (gauge_value > 180) 
				gauge_value = 0;
			
			lv_gauge_set_value(guider_ui.screen_gauge_1, 0, gauge_value);
			gauge_value -= 10;
		}
		break;
		
	default:
		break;
	}
}

void events_init_screen(lv_ui *ui)
{
	lv_obj_set_event_cb(ui->screen_btn_1, screen_btn_1event_handler);
	lv_obj_set_event_cb(ui->screen_btn_2, screen_btn_2event_handler);
}
