/*
 * Copyright 2022 NXP
 * SPDX-License-Identifier: MIT
 */

#include "lvgl/lvgl.h"
#include <stdio.h>
#include "gui_guider.h"
#include "events_init.h"
#include "custom.h"


void setup_scr_screen(lv_ui *ui){

	//Write codes screen
	ui->screen = lv_obj_create(NULL, NULL);

	//Write style LV_OBJ_PART_MAIN for screen
	static lv_style_t style_screen_main;
	lv_style_reset(&style_screen_main);

	//Write style state: LV_STATE_DEFAULT for style_screen_main
	lv_style_set_bg_color(&style_screen_main, LV_STATE_DEFAULT, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_bg_opa(&style_screen_main, LV_STATE_DEFAULT, 0);
	lv_obj_add_style(ui->screen, LV_OBJ_PART_MAIN, &style_screen_main);
	lv_anim_path_t lv_anim_path_screen_btn_1 = {.cb = lv_anim_path_linear};

	//Write codes screen_btn_1
	ui->screen_btn_1 = lv_btn_create(ui->screen, NULL);

	//Write style LV_BTN_PART_MAIN for screen_btn_1
	static lv_style_t style_screen_btn_1_main;
	lv_style_reset(&style_screen_btn_1_main);

	//Write style state: LV_STATE_DEFAULT for style_screen_btn_1_main
	lv_style_set_radius(&style_screen_btn_1_main, LV_STATE_DEFAULT, 50);
	lv_style_set_bg_color(&style_screen_btn_1_main, LV_STATE_DEFAULT, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_bg_grad_color(&style_screen_btn_1_main, LV_STATE_DEFAULT, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_bg_grad_dir(&style_screen_btn_1_main, LV_STATE_DEFAULT, LV_GRAD_DIR_NONE);
	lv_style_set_bg_opa(&style_screen_btn_1_main, LV_STATE_DEFAULT, 255);
	lv_style_set_shadow_color(&style_screen_btn_1_main, LV_STATE_DEFAULT, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_shadow_width(&style_screen_btn_1_main, LV_STATE_DEFAULT, 0);
	lv_style_set_shadow_opa(&style_screen_btn_1_main, LV_STATE_DEFAULT, 255);
	lv_style_set_shadow_spread(&style_screen_btn_1_main, LV_STATE_DEFAULT, 0);
	lv_style_set_shadow_ofs_x(&style_screen_btn_1_main, LV_STATE_DEFAULT, 0);
	lv_style_set_shadow_ofs_y(&style_screen_btn_1_main, LV_STATE_DEFAULT, 0);
	lv_style_set_border_color(&style_screen_btn_1_main, LV_STATE_DEFAULT, lv_color_make(0x01, 0xa2, 0xb1));
	lv_style_set_border_width(&style_screen_btn_1_main, LV_STATE_DEFAULT, 2);
	lv_style_set_border_opa(&style_screen_btn_1_main, LV_STATE_DEFAULT, 255);
	lv_style_set_text_color(&style_screen_btn_1_main, LV_STATE_DEFAULT, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_text_font(&style_screen_btn_1_main, LV_STATE_DEFAULT, &lv_font_simsun_12);
	lv_obj_add_style(ui->screen_btn_1, LV_BTN_PART_MAIN, &style_screen_btn_1_main);
	lv_obj_set_pos(ui->screen_btn_1, 0, -2);
	lv_obj_set_size(ui->screen_btn_1, 85, 36);

	//Write animation: screen_btn_1move in y direction
	lv_anim_t screen_btn_1_y;
	lv_anim_init(&screen_btn_1_y);
	lv_anim_set_var(&screen_btn_1_y, ui->screen_btn_1);
	lv_anim_set_time(&screen_btn_1_y, 100);
	lv_anim_set_exec_cb(&screen_btn_1_y, (lv_anim_exec_xcb_t)lv_obj_set_y);
	lv_anim_set_values(&screen_btn_1_y, lv_obj_get_y(ui->screen_btn_1), 0);
	_lv_memcpy_small(&screen_btn_1_y.path, &lv_anim_path_screen_btn_1, sizeof(lv_anim_path_cb_t));
	lv_anim_start(&screen_btn_1_y);

	ui->screen_btn_1_label = lv_label_create(ui->screen_btn_1, NULL);
	lv_label_set_text(ui->screen_btn_1_label, "Button");

	//Write codes screen_gauge_1
	ui->screen_gauge_1 = lv_gauge_create(ui->screen, NULL);

	//Write style LV_GAUGE_PART_MAIN for screen_gauge_1
	static lv_style_t style_screen_gauge_1_main;
	lv_style_reset(&style_screen_gauge_1_main);

	//Write style state: LV_STATE_DEFAULT for style_screen_gauge_1_main
	lv_style_set_bg_color(&style_screen_gauge_1_main, LV_STATE_DEFAULT, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_bg_grad_color(&style_screen_gauge_1_main, LV_STATE_DEFAULT, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_bg_grad_dir(&style_screen_gauge_1_main, LV_STATE_DEFAULT, LV_GRAD_DIR_NONE);
	lv_style_set_bg_opa(&style_screen_gauge_1_main, LV_STATE_DEFAULT, 255);
	lv_style_set_text_color(&style_screen_gauge_1_main, LV_STATE_DEFAULT, lv_color_make(0x39, 0x3c, 0x41));
	lv_style_set_text_font(&style_screen_gauge_1_main, LV_STATE_DEFAULT, &lv_font_simsun_12);
	lv_style_set_text_letter_space(&style_screen_gauge_1_main, LV_STATE_DEFAULT, 0);
	lv_style_set_pad_inner(&style_screen_gauge_1_main, LV_STATE_DEFAULT, 15);
	lv_style_set_line_color(&style_screen_gauge_1_main, LV_STATE_DEFAULT, lv_color_make(0x8b, 0x89, 0x8b));
	lv_style_set_line_width(&style_screen_gauge_1_main, LV_STATE_DEFAULT, 3);
	lv_style_set_line_opa(&style_screen_gauge_1_main, LV_STATE_DEFAULT, 255);
	lv_style_set_scale_grad_color(&style_screen_gauge_1_main, LV_STATE_DEFAULT, lv_color_make(0x8b, 0x89, 0x8b));
	lv_style_set_scale_end_color(&style_screen_gauge_1_main, LV_STATE_DEFAULT, lv_color_make(0x00, 0xa1, 0xb5));
	lv_style_set_scale_width(&style_screen_gauge_1_main, LV_STATE_DEFAULT, 9);
	lv_style_set_scale_border_width(&style_screen_gauge_1_main, LV_STATE_DEFAULT, 0);
	lv_style_set_scale_end_border_width(&style_screen_gauge_1_main, LV_STATE_DEFAULT, 5);
	lv_style_set_scale_end_line_width(&style_screen_gauge_1_main, LV_STATE_DEFAULT, 4);
	lv_obj_add_style(ui->screen_gauge_1, LV_GAUGE_PART_MAIN, &style_screen_gauge_1_main);

	//Write style LV_GAUGE_PART_MAJOR for screen_gauge_1
	static lv_style_t style_screen_gauge_1_major;
	lv_style_reset(&style_screen_gauge_1_major);

	//Write style state: LV_STATE_DEFAULT for style_screen_gauge_1_major
	lv_style_set_line_color(&style_screen_gauge_1_major, LV_STATE_DEFAULT, lv_color_make(0x8b, 0x89, 0x8b));
	lv_style_set_line_width(&style_screen_gauge_1_major, LV_STATE_DEFAULT, 5);
	lv_style_set_line_opa(&style_screen_gauge_1_major, LV_STATE_DEFAULT, 255);
	lv_style_set_scale_grad_color(&style_screen_gauge_1_major, LV_STATE_DEFAULT, lv_color_make(0x8b, 0x89, 0x8b));
	lv_style_set_scale_end_color(&style_screen_gauge_1_major, LV_STATE_DEFAULT, lv_color_make(0x00, 0xa1, 0xb5));
	lv_style_set_scale_width(&style_screen_gauge_1_major, LV_STATE_DEFAULT, 16);
	lv_style_set_scale_end_line_width(&style_screen_gauge_1_major, LV_STATE_DEFAULT, 5);
	lv_obj_add_style(ui->screen_gauge_1, LV_GAUGE_PART_MAJOR, &style_screen_gauge_1_major);

	//Write style LV_GAUGE_PART_NEEDLE for screen_gauge_1
	static lv_style_t style_screen_gauge_1_needle;
	lv_style_reset(&style_screen_gauge_1_needle);

	//Write style state: LV_STATE_DEFAULT for style_screen_gauge_1_needle
	lv_style_set_size(&style_screen_gauge_1_needle, LV_STATE_DEFAULT, 20);
	lv_style_set_bg_color(&style_screen_gauge_1_needle, LV_STATE_DEFAULT, lv_color_make(0x41, 0x48, 0x5a));
	lv_style_set_bg_grad_color(&style_screen_gauge_1_needle, LV_STATE_DEFAULT, lv_color_make(0x41, 0x48, 0x5a));
	lv_style_set_bg_grad_dir(&style_screen_gauge_1_needle, LV_STATE_DEFAULT, LV_GRAD_DIR_NONE);
	lv_style_set_bg_opa(&style_screen_gauge_1_needle, LV_STATE_DEFAULT, 255);
	lv_style_set_pad_inner(&style_screen_gauge_1_needle, LV_STATE_DEFAULT, 0);
	lv_style_set_line_width(&style_screen_gauge_1_needle, LV_STATE_DEFAULT, 4);
	lv_obj_add_style(ui->screen_gauge_1, LV_GAUGE_PART_NEEDLE, &style_screen_gauge_1_needle);
	lv_obj_set_pos(ui->screen_gauge_1, 10, 40);
	lv_obj_set_size(ui->screen_gauge_1, 200, 200);
	lv_gauge_set_scale(ui->screen_gauge_1, 300, 37, 19);
	lv_gauge_set_range(ui->screen_gauge_1, 0, 180);
	static lv_color_t needle_colors_screen_gauge_1[1];
	needle_colors_screen_gauge_1[0] = lv_color_make(0xff, 0x00, 0x00);
	lv_gauge_set_needle_count(ui->screen_gauge_1, 1, needle_colors_screen_gauge_1);
	lv_gauge_set_critical_value(ui->screen_gauge_1, 120);
	lv_gauge_set_value(ui->screen_gauge_1, 0, 0);

	//Write codes screen_btn_2
	ui->screen_btn_2 = lv_btn_create(ui->screen, NULL);

	//Write style LV_BTN_PART_MAIN for screen_btn_2
	static lv_style_t style_screen_btn_2_main;
	lv_style_reset(&style_screen_btn_2_main);

	//Write style state: LV_STATE_DEFAULT for style_screen_btn_2_main
	lv_style_set_radius(&style_screen_btn_2_main, LV_STATE_DEFAULT, 50);
	lv_style_set_bg_color(&style_screen_btn_2_main, LV_STATE_DEFAULT, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_bg_grad_color(&style_screen_btn_2_main, LV_STATE_DEFAULT, lv_color_make(0xff, 0xff, 0xff));
	lv_style_set_bg_grad_dir(&style_screen_btn_2_main, LV_STATE_DEFAULT, LV_GRAD_DIR_NONE);
	lv_style_set_bg_opa(&style_screen_btn_2_main, LV_STATE_DEFAULT, 255);
	lv_style_set_shadow_color(&style_screen_btn_2_main, LV_STATE_DEFAULT, lv_color_make(0x21, 0x95, 0xf6));
	lv_style_set_shadow_width(&style_screen_btn_2_main, LV_STATE_DEFAULT, 0);
	lv_style_set_shadow_opa(&style_screen_btn_2_main, LV_STATE_DEFAULT, 255);
	lv_style_set_shadow_spread(&style_screen_btn_2_main, LV_STATE_DEFAULT, 0);
	lv_style_set_shadow_ofs_x(&style_screen_btn_2_main, LV_STATE_DEFAULT, 0);
	lv_style_set_shadow_ofs_y(&style_screen_btn_2_main, LV_STATE_DEFAULT, 0);
	lv_style_set_border_color(&style_screen_btn_2_main, LV_STATE_DEFAULT, lv_color_make(0x01, 0xa2, 0xb1));
	lv_style_set_border_width(&style_screen_btn_2_main, LV_STATE_DEFAULT, 2);
	lv_style_set_border_opa(&style_screen_btn_2_main, LV_STATE_DEFAULT, 255);
	lv_style_set_text_color(&style_screen_btn_2_main, LV_STATE_DEFAULT, lv_color_make(0x00, 0x00, 0x00));
	lv_style_set_text_font(&style_screen_btn_2_main, LV_STATE_DEFAULT, &lv_font_simsun_12);
	lv_obj_add_style(ui->screen_btn_2, LV_BTN_PART_MAIN, &style_screen_btn_2_main);
	lv_obj_set_pos(ui->screen_btn_2, 156, 0);
	lv_obj_set_size(ui->screen_btn_2, 82, 34);
	ui->screen_btn_2_label = lv_label_create(ui->screen_btn_2, NULL);
	lv_label_set_text(ui->screen_btn_2_label, "test");

	//Init events for screen
	events_init_screen(ui);
}