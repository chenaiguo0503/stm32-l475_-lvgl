#include "sys.h"
#include "usart.h"
#include "delay.h"
#include "led.h"
#include "lcd.h"
#include "lvgl.h"
#include "lv_port_disp.h"
#include "lv_port_indev.h"
#include "gui_guider.h"
#include "events_init.h"

lv_ui guider_ui;

int main(void)
{
    u8 color = 0;
	
    HAL_Init();
    SystemClock_Config();	//初始化系统时钟为80M
    delay_init(80); 		//初始化延时函数    80M系统时钟
    uart_init(115200);		//初始化串口，波特率为115200

    LED_Init();				//初始化LED

	lv_init();
	lv_port_disp_init();
	lv_port_indev_init();
	
    setup_ui(&guider_ui);
	
    while(1)
    {
		lv_task_handler();
    }
}


